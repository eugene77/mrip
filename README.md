﻿# mRIP data compressor

mRIP is a data compressor developed by Dmitriy Bystrov on ZX Spectrum platform. It is a simplified version of [RIP](https://gitlab.com/eugene77/rip) compressor, implementing LZ + Huffman scheme (see [format description](format_description.txt) for details) while keeping decompressor size small.

This project contains powerful compressor implementation for PC platform. This implementation uses dynamic programming + exhaustive search to achieve compression level close to optimal for mRIP coding scheme.

This compressor produces raw compressed stream, no headers, no decompressor included. 

### Decompressor

Use `-d` option to decompress files.

[z80](z80) directory contains a few decompressor implementations for Z80 platform.

All implementations require working area of 1474 bytes.

### Legal

Compressor is published under [custom license](LICENSE). Z80 decompressors are published without license.

### See also

* [Real Information Packer](https://gitlab.com/eugene77/rip)
