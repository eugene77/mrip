﻿#pragma once

#include "types.h"
#include <vector>
#include <functional>

using namespace std;

extern vector<byte> compress_medium(const vector<byte>& data_x2, int datasize_x1, std::function<void(vector<byte>&)>& saveResult);

